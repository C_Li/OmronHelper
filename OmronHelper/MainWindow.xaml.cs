﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OmronHelper
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly System.Timers.Timer timer = new System.Timers.Timer(1000);
        private readonly OmronLib.Omron omron = new OmronLib.Omron();

        public MainWindow()
        {
            InitializeComponent();

            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }


        /// <summary>
        /// 用个timer来更新界面上的灯
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                StatusLight.Fill = omron.IsConnected ? new SolidColorBrush(Colors.LimeGreen) : new SolidColorBrush(Colors.Red);
            }));

        }

        /// <summary>
        /// 连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(IPBox.Text) || string.IsNullOrEmpty(PortBox.Text))
            {
                MessageBox.Show("IP和Port不能为空！", "连接失败");
            }
            else
            {
                omron.IPAddr = IPAddress.Parse(IPBox.Text);
                int port;
                if (int.TryParse(PortBox.Text, out port))
                {
                    omron.Port = port;
                }

                string info;
                if (!omron.PlcConnect(out info))
                    MessageBox.Show(info, "连接失败");
                else
                {
                    ReadBtn.IsEnabled = true;
                    ReadBitBtn.IsEnabled = true;
                    WriteBtn.IsEnabled = true;
                }
            }
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisconnectBtn_Click(object sender, RoutedEventArgs e)
        {
            omron.DisConnect();
            ReadBtn.IsEnabled = false;
            ReadBitBtn.IsEnabled = false;
            WriteBtn.IsEnabled = false;
        }

        /// <summary>
        /// 读字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadBtn_Click(object sender, RoutedEventArgs e)
        {
            // 从D8100读到D8115
            string[] res;
            omron.ReadDWords(8100, 16, out res);

            // 输出
            ValueBox.AppendText("读字 D8100-D8115     ");
            foreach (string item in res)
            {
                ValueBox.AppendText(item);
                ValueBox.AppendText(" ");
            }
            ValueBox.AppendText("\n");
        }

        /// <summary>
        /// 读位
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadBitBtn_Click(object sender, RoutedEventArgs e)
        {
            // 从D8100.0读到D8100.15
            string[] res;
            omron.ReadDBits(8100, 0, 16, out res);

            // 输出
            ValueBox.AppendText("读位 D8100.0-D8100.15    ");
            foreach (string item in res)
            {
                ValueBox.AppendText(item);
                ValueBox.AppendText(" ");
            }
            ValueBox.AppendText("\n");
        }

        /// <summary>
        /// 写
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WriteBtn_Click(object sender, RoutedEventArgs e)
        {
            //把D8100的16个位全写1
            //omron.WriteDBits(8100, 0, 16, new bool[] { true, true,true, true, true, true, true, true, true, true, true, true, true, true, true, true });
            
            //把D8100的第一个位写0
            //omron.WriteDBits(8100, 0, 1, new bool[] {false });

            //把D8100写0
            omron.WriteDWords(8100, 1, new int[] { 0 });
        }

    }
}

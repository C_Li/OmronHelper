﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OmronLib
{
    public static class DataTools
    {
        /// <summary>
        /// 将byte类型数据数组转换为16进制字符串形式
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string ByteToHexString(byte[] hex, int len)
        {
            string returnstr = "";
            for (int i = 0; i < len; i++)
            {
                returnstr += hex[i].ToString("X2");
            }
            return returnstr;
        }

        /// <summary>
        /// 将一个长字符串转化为每个元素包含两个字符的字符数组
        /// </summary>
        /// <param name="src"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        public static string[] StrToStrArray(string src)
        {
            try
            {
                string[] res = new string[src.Length / 2];

                for (int i = 0; i < src.Length / 2; i++)
                {
                    res[i] = src.Substring(i * 2, 2);
                }

                return res;
            }
            catch
            {
                return null;
            }
        }
    }
}
